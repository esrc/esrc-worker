ESRC RQ Worker Client Interface (aka Dashboard)
===============================================

Provides a web interface for end users to select preconfigured jobs for
execution using the RQ job queue.

Credits
-------

Dashboard is a project of the eScholarship Research Center at the University
of Melbourne. For more information about the project, please contact us at:

 > eScholarship Research Center
 > University of Melbourne
 > Parkville, Victoria
 > Australia
 > www.esrc.unimelb.edu.au

Authors:

 * Davis Marques <davis.marques@unimelb.edu.au>

Thanks:

 * Pyramid - http://www.pylonsproject.org/
 * Python Redis - https://pypi.python.org/pypi/redis
 * Redis - http://redis.io/
 * RQ - http://python-rq.org/


License
-------

Please see the LICENSE file for license information.


Revision History
----------------

0.2.0

- Send jobs to named queues.


0.1.0

- First iteration.

