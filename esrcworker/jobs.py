"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import ConfigParser
import logging
import utils


TIMEOUT_SHORT_JOB = 300
TIMEOUT_STANDARD_JOB = 600
TIMEOUT_LONG_JOB = 3600


logger = logging.getLogger()

CAT = '/bin/cat'
ECHO_BIN = '/bin/echo'
etc = 'etc'
INDEXER = '/usr/share/eaccpf-indexer/Indexer/Indexer.py'
MAILER = '/usr/bin/mail'
mailer_config = 'mailer.ini'
project_config = 'project.ini'
python = '/usr/local/scholarly-python2/bin/python'
rsync = '/usr/bin/rsync'
web = '/srv/ha/web'


# DEFINING NEW JOBS
#
# Jobs are identified by naming them following the convention
# "job_lower_case_name". Each job must return a tuple consisting of a shell
# command to be executed, the name of a queue on which the job should be posted
# and a timeout value for the job.
#
# Only one shell command is allowed. However, multiple commands may be
# provided by concatenating them together with a semicolon, into a single
# string value.
#
# Queues are named after the host that should execute the job. For example, the
# host 'idx.internal' executes jobs that are posted to the 'idx.internal' queue.
# The default queue is not used in our implementation.
#
# The job timeout value is 180 seconds by default. After this period has expired,
# rqworker considers the job to be hung or have failed. For long running jobs
# like indexing, this value is too short and an explicit timeout value, defined
# in seconds, must be provided.
#

def _job_create_mailer_configuration(Project, Email, JobName):
    """
    Create a mailer configuration for the selected project. The mailer is configured with
    a number of default settings. The mailer.internal application will discover the
    configuration file automatically, so there is no need to signal it to load the file.
    @todo mailer config files have been moved to puppet. this job needs to be updated or removed.
    """
    mailer = '{0}/{1}/{2}/{3}'.format(web, Project, etc, mailer_config)
    config = ConfigParser.ConfigParser()
    config.add_section("mailer")
    config.set('mailer', 'sitename', 'sitename')
    config.set('mailer', 'from', 'project@unimelb.edu.au')
    config.set('mailer', 'mailto', 'youraddress@unimelb.edu.au; youraddress2@unimelb.edu.au')
    config.set('mailer', 'subject', 'Thank-you for Your Message')
    config.set('mailer', 'thankyou', 'Thank-you for contacting us. We have received your message and will respond as soon as possible.\n\n-- Project Team')
    config.set('mailer', 'successpage', 'http://www.esrc.unimelb.edu.au/success.html')
    config.set('mailer', 'errorpage', 'http://www.esrc.unimelb.edu.au/failure.html')
    try:
        with open(mailer, 'w') as configfile:
            config.write(configfile)
    except:
        return ''

def _job_list_directory(Project, Email, JobName):
    """
    This job is for testing only.
    """
    queue_name = 'idx.internal'
    job_log = utils.getLogFilePath(Project, JobName)
    timeout = TIMEOUT_SHORT_JOB
    # list directory contents job
    ls_JOB = "ls -l /srv/ha/web/{0} &>> {1}".format(Project, job_log)
    # notification message
    subject = '{0} site migration job is complete'.format(Project)
    email_JOB = """{0} {1} | {2} -s "{3}" {4}""".format(CAT, job_log, MAILER, subject, Email)
    # return command
    cmd = [ls_JOB, email_JOB]
    cmd = ';'.join(cmd)
    log_msg = ("Starting {0} for project {1} with '{2}'").format(JobName, Project, cmd)
    logger.info(log_msg)
    return cmd, queue_name, timeout

def _job_migrate_testing_site_to_live_site(Project, Email, JobName):
    """
    Migrate testing content to the live folder for the selected project. Send an
    email to the specified address when the job is complete.
    """
    queue_name = 'worker.internal'
    job_log = utils.getLogFilePath(Project, JobName)
    timeout = TIMEOUT_SHORT_JOB
    # rsync job
    source = '{0}/{1}/testing/'.format(web, Project)
    dest = '{0}/{1}/LIVE/'.format(web, Project)
    rsync_JOB = "{0} {1} {2} {3} {4} > {5}".format(rsync, '-rtv', '--delete', source, dest, job_log)
    # notification message
    subject = '{0} site migration job is complete'.format(Project)
    email_JOB = """{0} {1} | {2} -s "{3}" {4}""".format(CAT, job_log, MAILER, subject, Email)
    # return command
    cmd = [rsync_JOB, email_JOB]
    cmd = ';'.join(cmd)
    log_msg = ("Starting {0} for project {1} with '{2}'").format(JobName, Project, cmd)
    logger.info(log_msg)
    return cmd, queue_name, timeout

def _job_update_network_graph(Project, Email, JobName):
    """
    Update the network map for the specified project.
    """
    queue_name = 'worker.internal'
    job_log = utils.getLogFilePath(Project, JobName)
    timeout = TIMEOUT_STANDARD_JOB
    # update analysis
    config = '/etc/indexer/all/' + Project
    options = '--graph --loglevel INFO'
    indexing_JOB = '{0} {1} {2} {3} > {4}'.format(python, INDEXER, config, options, job_log)
    # send email
    subject = '{0} collection network graphing job is complete'.format(Project)
    email_JOB = """{0} {1} | {2} -s "{3}" {4}""".format(CAT, job_log, MAILER, subject, Email)
    # return command
    cmd = [indexing_JOB, email_JOB]
    cmd = ';'.join(cmd)
    log_msg = ("Starting {0} for project {1} with '{2}'").format(JobName, Project, cmd)
    logger.info(log_msg)
    return cmd, queue_name, timeout

def job_update_quality_analysis(Project, Email, JobName):
    """
    Analyze the collection content for quality indicators, then write an
    HTML report to the testing/report folder. Send an email to the
    specified address when the job is complete. The job takes approximately
    2 to 5 minutes to complete.
    """
    queue_name = 'idx.internal'
    job_log = utils.getLogFilePath(Project, JobName)
    timeout = TIMEOUT_STANDARD_JOB
    # update analysis
    config = '/etc/indexer/all/' + Project
    options = '--analyze --loglevel INFO'
    indexing_JOB = '{0} {1} {2} {3} > {4}'.format(python, INDEXER, config, options, job_log)
    # send email
    subject = '{0} collection analysis job is complete'.format(Project)
    email_JOB = """{0} {1} | {2} -s "{3}" {4}""".format(CAT, job_log, MAILER, subject, Email)
    # return command
    cmd = [indexing_JOB, email_JOB]
    cmd = ';'.join(cmd)
    log_msg = ("Starting {0} for project {1} with '{2}'").format(JobName, Project, cmd)
    logger.info(log_msg)
    return cmd, queue_name, timeout

def job_update_search_index(Project, Email, JobName):
    """
    Update the search index for the selected project and then send an
    email to the specified address when the job is complete. The document
    search index enables users to locate pages and page content by search term,
    much like Google search. This job takes approximately 5 to 30 minutes to
    complete.
    """
    queue_name = 'idx.internal'
    job_log = utils.getLogFilePath(Project, JobName)
    timeout = TIMEOUT_LONG_JOB
    # update analysis
    config_path = '/etc/indexer/all/' + Project
    options = '--crawl --clean --infer --transform --post --loglevel INFO'
    indexing_job = '{0} {1} {2} {3} > {4}'.format(python, INDEXER, config_path, options, job_log)
    # send email
    subject = '{0} document indexing job is complete'.format(Project)
    email_job = """{0} {1} | {2} -s "{3}" {4}""".format(CAT, job_log, MAILER, subject, Email)
    # return command
    cmd = [indexing_job, email_job]
    cmd = ';'.join(cmd)
    log_msg = ("Starting {0} for project {1} with '{2}'").format(JobName, Project, cmd)
    logger.info(log_msg)
    #return cmd, queue_name, timeout
    return cmd, queue_name, timeout
