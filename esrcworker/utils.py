"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import ConfigParser
import datetime
import logging
import os
import jobs as project_jobs

LOGS_PATH = '/srv/ha/web/logs'

log = logging.getLogger()


#------------------------------------------------------------------------------

def _arrayToString(Value):
    """Convert array representation into a string.
    """
    val = Value[1:-1]
    s = []
    items = val.split(",")
    for token in items:
        token = token.lstrip().rstrip()
        if token.startswith('u\''):
            token = token.replace('u\'', '', 1)
        token = token.lstrip().rstrip()
        if token.startswith("'"):
            token = token[1:]
        if token.endswith("'"):
            token = token[:-1]
        s.append(token)
    return ', '.join(s)

def getConfiguration(Path):
    """Get contents of configuration file as a dictionary."""
    try:
        config = ConfigParser.SafeConfigParser()
        config.read(Path)
        cfg = {}
        for item in config.items('mailer'):
            key, value = item
            if value.startswith('['):
                cfg[key] = _arrayToString(value)
            else:
                cfg[key] = value
        return cfg
    except:
        return None

def getDateStampFromFilename(Filename):
    """
    Get the date stamp portion of the log file name.
    2013_12_25_12_20_36
    """
    date = Filename[:10].replace("_","-")
    time = Filename[11:19].replace("_",":")
    return "{0} {1}".format(date, time)

def getIndexerConfiguration(Settings, ProjectCode):
    """Get the indexer configuration."""
    project_path = Settings['project_path']
    project_config_path = Settings['project_config_path']
    indexer_config_file = Settings['indexer_config_file']
    indexer_config_path = project_path + os.sep + ProjectCode + os.sep + project_config_path + os.sep + indexer_config_file
    try:
        f = open(indexer_config_path)
        lines = f.readlines()
        f.close()
        return ''.join(lines)
    except:
        return None

def getJobIdFromFilename(Filename):
    """
    Get the task datestamp from the filename.
    """
    return Filename[:19]

def getJobList():
    """
    Get the list of predefined tasks from the tasks module.
    """
    items = []
    try:
        for item in dir(project_jobs):
            if item.startswith('job_'):
                name = item[4:].replace('_', ' ').title()
                desc = getattr(project_jobs, item).__doc__
                job = {
                    "id": item,
                    "name": name,
                    "description" : desc,
                }
                items.append(job)
    except:
        pass
    return items

def getJobName(Name):
    """
    Get task name in title case, with underscores and task_ removed.
    """
    return Name[4:].replace('_', ' ').title()

def getJobNameFromLogFile(Filename):
    """
    Get the task name from a log filename.
    """
    parts = Filename.split("-")
    task_name = parts[2].replace('.log','').replace('task_','').replace('_',' ')
    return task_name

def getLogFilePath(Project, Task):
    """
    Generate a unique log file name that incorporates the specified project
    code and task name.
    """
    now = str(datetime.datetime.now()).replace(" ","_").replace(":","_").replace("-","_")
    i = now.rfind(".")
    now = now[:i]
    return "{0}{1}{2}-{3}-{4}.log".format(LOGS_PATH, os.sep, now, Project, Task)

def getMailerConfiguration(Settings, ProjectCode):
    """Get the project mailer configuration. Return None if the project or mailer
    configuration could not be found."""
    project_path = Settings['project_path']
    project_config_path = Settings['project_config_path']
    mailer_config_file = Settings['mailer_config_file']
    mailer_config_path = project_path + os.sep + ProjectCode + os.sep + project_config_path + os.sep + mailer_config_file
    try:
        config = ConfigParser.SafeConfigParser()
        config.read(mailer_config_path)
        cfg = {}
        for item in config.items('mailer'):
            key, value = item
            if value.startswith('['):
                cfg[key] = _arrayToString(value)
            else:
                cfg[key] = value
        return cfg
    except:
        return None

def getProjectCodeFromLogFile(Filename):
    """
    Extract the project code from a log file name
    """
    parts = Filename.split("-")
    return parts[1]

def getProjectCodes(Settings):
    """Get the list of projects in the projects directory.
    """
    excluded = ['.','.clumanager','lost+found']
    projects = []
    try:
        path = Settings['project_path']
        folders = os.listdir(path)
        for name in folders:
            if os.path.isdir(path + os.sep + name) and not name in excluded:
                projects.append(name)
        projects.sort()
    except:
        pass
    return projects

def getProjectMetadata(Settings, ProjectCode):
    """Get metadata for the specified project path.
    """
    metadata = {}
    # we return these as a default, to ensure a minimum amount of information is
    # available to the user
    metadata['code'] = ProjectCode
    metadata['title'] = ""
    metadata['description'] = ""
    # get the project paths
    project_path = Settings['project_path']
    project_config_path = Settings['project_config_path']
    project_config_file = Settings['project_config_file']
    # read the project configuration file
    config_path = project_path + os.sep + ProjectCode + os.sep + project_config_path + os.sep + project_config_file
    if not os.path.exists(config_path):
        msg = "Project metadata file does not exist for {0}".format(ProjectCode)
        log.warning(msg, exc_info=False)
        return metadata
    else:
        try:
            config = ConfigParser.SafeConfigParser()
            config.read(config_path)
            metadata = dict(config.items('project'))
        except:
            msg = "Could not read project metadata file for {0}".format(ProjectCode)
            log.error(msg, exc_info=False)
        finally:
            return metadata

def getProjectListWithMetadata(Settings):
    """Get the list of projects with their associated title, description metadata.
    """
    projects = []
    try:
        excluded_folders = ['.','.clumanager','logs','lost+found']
        excluded_projects = Settings['excluded_projects']
        project_path = Settings['project_path']
        for name in os.listdir(project_path):
            if os.path.isdir(project_path + os.sep + name) and not name in excluded_folders and not name in excluded_projects:
                try:
                    metadata = getProjectMetadata(Settings, name)
                    projects.append(metadata)
                except:
                    pass
        projects.sort()
    except Exception as e:
        log.error("{0}".format(e.message), exc_info=True)
    return projects

def setProjectMetadata(Settings, ProjectCode, Metadata):
    """Set metadata for the specified project.
    """
    project_path = Settings['project_path']
    project_config_path = Settings['project_config_path']
    project_config_file = Settings['project_config_file']
    config_path = project_path + os.sep + ProjectCode + os.sep + project_config_path + os.sep + project_config_file
    try:
        config = ConfigParser.SafeConfigParser()
        if os.path.exists(config_path):
            config.read(config_path)
        for key, value in Metadata.items():
            config.set('project', key, value)
        with open(config_path, 'w') as configfile:
            config.write(configfile)
        msg = "Updated project configuration for {0}".format(ProjectCode)
        log.info(msg)
    except:
        msg = "Could not update project configuration for {0}".format(ProjectCode)
        log.error(msg, exc_info=True)

