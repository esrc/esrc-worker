"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import os
import shutil
import jobs
import traceback
import unittest


class TestTasks(unittest.TestCase):
    """Test cases for the tasks module.
    """

    def setUp(self):
        '''
        Setup the test environment.
        '''
        pass

    def tearDown(self):
        '''
        Tear down the test environment.
        '''
        pass

    def test_task_update_collection_analysis(self):
        """
        It should execute the Indexer application to generate a new or updated
        index for a specified project. It should then send an email to a
        specified email address at the conclusion of the operation.
        """
        cases = [
            ('BFYA','dmarq.ezz@gmail.com'),
        ]
        for case in cases:
            project, email = case
            cache = '/var/lib/indexer/' + project + os.sep + 'analysis'
            # clear the existing project cache
            if os.path.exists(cache):
                shutil.rmtree(cache)
            os.mkdir(cache)
            self.assertEquals(os.path.exists(cache), True)
            # run the task
            try:
                jobs.task_update_collection_analysis(project, email)
            except:
                traceback.print_exc()
                self.fail('Task execution failed')
            # check the project directory to confirm that the analysis folder
            # contains updated content
            files = os.listdir(cache)
            self.assertGreater(len(files),0)

    def test_task_update_collection_network_graph(self):
        pass

    def test_task_update_document_search_index(self):
        pass

    def test_task_update_image_search_index(self):
        pass

    def test_task_update_location_search_index(self):
        pass

if __name__ == "__main__":
    unittest.main()