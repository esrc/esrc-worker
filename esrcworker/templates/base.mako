<!DOCTYPE html>
<html ng-app>
    <head>
        <meta charset="utf-8">
        <title>ESRC Site Manager</title>
        <meta name="author" content="eScholarship Research Centre, University of Melbourne">
        <meta name="description" content="ESRC web site manageer">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- stylesheets -->
        <link href="/static/css/bootstrap/bootstrap.css" rel="stylesheet">
        <link href="/static/css/screen.css" rel="stylesheet">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/static/js/html5shiv/html5shiv.js"></script>
        <![endif]-->
        <!-- scripts -->
        <script src="/static/js/jquery/jquery.min.js"></script>
        <script src="/static/js/bootstrap/bootstrap.min.js"></script>
        <script src="/static/js/angular/angular.min.js"></script>
        <script src="/static/js/app/app.js"></script>
    </head>
    <body>

    <header>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <a class="brand" href="/">Site Manager</a>-->
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <%block name="nav" />
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>

            </div>
        </div>

        <div class='container' style="padding-top:60px;">
        % if message:
            <div class="row">
                <div class="span12">
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        ${message}
                    </div>
                </div>
            </div>
        % endif
        % if errors and len(errors) > 0:
            <div class="row">
                <div class="span12">
                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <ul>
                            % for error in errors:
                            <li>${error}</li>
                            %endfor
                        </ul>
                    </div>
                </div>
            </div>
        % endif
        </div>
    </header>

    <section id="content">
        ${self.body()}
    </section>

    <footer>
        <div class="container" style="padding-bottom:40px;padding-top:20px">
            <div class="row">
                <div class="span12">
                    <footer>
                      <p>Copyright &copy; eScholarship Research Centre, University of Melbourne 2013</p>
                    </footer>
                </div>
            </div>
        </div>
    </footer>

  </body>
</html>
