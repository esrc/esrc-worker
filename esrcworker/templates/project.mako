<%inherit file="base.mako" />
<%block name="nav">
<li class="active"><a href="/projects">Projects</a></li>
<li><a href="/log">Log</a></li>
</%block>

<div class="container" xmlns="http://www.w3.org/1999/html">

    <!-- messages -->
    % if messages:
    <div class="row">
        <div class="span12">
            % for message in messages:
            <div class="alert alert-success">
                ${message}
            </div>
            % endfor
        </div>
    </div>
    % endif
    <!-- errors -->
    % if errors:
        <div class="row">
            <div class="span12">
                % for error in errors:
                <div class="alert alert-warning">
                    ${errors}
                </div>
                % endfor
            </div>
        </div>
    % endif
    <!-- banner -->
    <div class='row'>
        <div class='span12'>
            % if title != None and title != '-':
                <h1>${code} - ${title}</h1>
            % else:
                <h1>${code}</h1>
            % endif
            % if description != None and description != '-':
            <p>${description}</p>
            % endif
            <!-- editing form for project metadata -->
            <br/>
            <table class="table table-compressed">
                <!--
                <tr>
                    <td>Quality Analysis Report</td>
                    <td><code>http://dashboard.esrc.unimelb.edu.au/project/${code}/report</code></td>
                </tr>
                -->
                <tr>
                    <td>Development Web Site</td>
                    <td><code>https://web.esrc.unimelb.edu.au/${code}d</code></td>
                </tr>
                <tr>
                    <td>Live Web Site</td>
                    <td><code>https://web.esrc.unimelb.edu.au/${code}</code></td>
                </tr>
                <tr>
                    <td>Testing Web Site</td>
                    <td><code>https://web.esrc.unimelb.edu.au/${code}t</code></td>
                </tr>
                <tr>
                    <td>Search Index</td>
                    <td><code>http://data.esrc.unimelb.edu.au/solr/${code}</code></td>
                </tr>
            </table>
        </div>
    </div>

    <!-- project jobs -->
% if read_only == True:
    <br/>
    <div class="alert alert-info">
        This project can not be configured through the dashboard.  Please contact the system administrator for assistance.
    </div>
% else:
    <form action="/project/${code}" method="post">
    <div class="row">
        <div class="span9">
            <h3>Jobs</h3>
        </div>
    </div>
    <div class='row'>
        <div class="span9">
            <table class="table table-compressed jobs">
                % for job in jobs:
                <tr>
                    <td><input type="checkbox" name="${job['id']}" /></td>
                    <td>
                        <h4>${job['name']}</h4>
                        <p>${job['description']}</p>
                    </td>
                </tr>
                % endfor
            </table>
        </div>
        <div class="span3">
            <div class="well">
                <p>
                    Select a project to execute the job(s) on. An email
                    will be sent to the specified address when the job has
                    completed.
                </p>
                <fieldset>
                    <label label-for="email">Email Address</label>
                    <input type="email" name="email" class="input-medium" placeholder="Email" required />
                    <input type="submit" class="btn btn-primary" value="Run Jobs" style="margin-top:10px;"/>
                </fieldset>
            </div>
        </div>
    </div>
    </form>
% endif

</div>
