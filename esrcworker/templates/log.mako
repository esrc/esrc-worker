<%inherit file="base.mako" />
<%block name="nav">
    <li><a href="/projects">Projects</a></li>
    <li class="active"><a href="/log">Log</a></li>
</%block>
<div class="container">
    <div class="row">
        <div class="span12">
            <h3>Log</h3>
            <p>Task log files are deleted once they are more than two weeks old.</p>

            <div class="panel-group" id="accordion">
            % for record in log:
              <div class="panel panel-default logfile">
                <div class="panel-heading">
                  <h5 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#${record['id']}">
                      ${record['datestamp']} - ${record['project']} - ${record['title']}
                    </a>
                  </h5>
                </div>
                <div id="${record['id']}" class="panel-collapse collapse">
                  <div class="panel-body">
                      % for line in record['output']:
                      <div class="line">${line}</div>
                      % endfor
                  </div>
                </div>
              </div>
            % endfor
            </div>

        </div>
    </div>
</div>
