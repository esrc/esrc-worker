<%inherit file="base.mako" />
<%block name="nav">
<li class="active"><a href="/projects">Projects</a></li>
<li><a href="/log">Log</a></li>
</%block>

<div class="container">
    <div class="row">
        <div class="span9">
            <h3>Project Web Sites</h3>
        </div>
    </div>
    <div class="row">
        <div class="span9">
            <table class="table table-condensed">
                <thead>
                <th>Code</th>
                <th>Title</th>
                <!-- <th>Description</th> -->
                </thead>
            % for project in projects:
            <tr>
                <td><a href="/project/${project['code']}">${project['code']}</a></td>
                <td>${project['title']}</td>
                <!-- <td>${project['description']}</td> -->
            </tr>
            % endfor
            </table>
        </div>
        <div class="span3">
            <div class="well">
                <a href="/project/create">Create a New Project</a>
            </div>
        </div>
    </div>
</div>
