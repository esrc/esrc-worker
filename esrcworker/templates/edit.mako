<%inherit file="base.mako" />
<%block name="nav">
<li class="active"><a href="/projects">Projects</a></li>
<li><a href="/log">Log</a></li>
</%block>

<div class="container">
    <div class="row">
        <div class="span12">
            <h3>Edit Project Metadata</h3>
        </div>
    </div>
    <div class="row">
        <div class="span6">

            <!-- edit project -->
            <form action="/project/${code}/edit" method="post">
                <div class="control-group">
                    <label class="control-label" for="code">Project Code</label>
                    <div class="controls">
                        <input type="text" name="code" placeholder="Four Letter Project Code" class="input-xlarge" value="${code}" readonly />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="title">Title</label>
                    <div class="controls">
                        <input type="text" name="title" placeholder="Title" class="input-xlarge" value="${title}" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="description">Description</label>
                    <div class="controls">
                        <textarea name="description" placeholder="Short Description" class="input-xlarge" rows="6">${description}</textarea>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value="Update">

            </form>
            <!-- /edit project -->

        </div>
        <div class="span6">
        </div>
    </div>
</div>

