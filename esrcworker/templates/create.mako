<%inherit file="base.mako" />
<%block name="nav">
<li class="active"><a href="/projects">Projects</a></li>
<li><a href="/log">Log</a></li>
</%block>

<div class="container">
    <div class="row">
        <div class="span12">
            <h3>Create a New Web Project Folder</h3>
        </div>
    </div>
    <div class="row">
        <div class="span6">

            <!-- create new project -->
            <form action="/project/create" method="post">
                <div class="control-group">
                    <label class="control-label" for="code">Project Code</label>
                    <div class="controls">
                        <input type="text" name="code" placeholder="Four Letter Project Code" ng-model="projectcode" class="input-xlarge">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="title">Title</label>
                    <div class="controls">
                        <input type="text" name="title" placeholder="Title" class="input-xlarge" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="description">Description</label>
                    <div class="controls">
                        <textarea name="description" placeholder="Short Description" class="input-xlarge" rows="6"></textarea>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" value="Create Project">

            </form>
            <!-- /create new project -->

        </div>
        <div class="span6">
            <p>
                This form will generate a new project folder named: "<code>{{projectcode}}</code>".
            </p>
            <p>
                Corresponding development, testing and live sites will be available online here:
            </p>

            <table>
                <tr>
                    <td>Development Site:</td>
                    <td class="pad-left"><code>https://web.esrc.unimelb.edu.au/{{projectcode}}d</code></td>
                </tr>
                <tr>
                    <td>Testing Site:</td>
                    <td class="pad-left"><code>https://web.esrc.unimelb.edu.au/{{projectcode}}t</code></td>
                </tr>
                <tr>
                    <td>Live Site:</td>
                    <td class="pad-left"><code>https://web.esrc.unimelb.edu.au/{{projectcode}}l</code></td>
                </tr>
            </table>

        </div>
    </div>
</div>

