"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from pyramid.config import Configurator
import ConfigParser
import logging
import os

# configure logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def main(global_config, **settings):
    """Return the Pyramid WSGI application.
    """
    # try to load the external config file
    props = ['excluded_projects',
             'read_only_projects',
             'no_execute_projects',
             'log_path',
             'indexer_config_file',
             'mailer_config_file',
             'project_config_file',
             'project_config_path',
             'project_path']
    try:
        config_file = global_config['here'] + os.sep + 'dashboard.ini'
        parser = ConfigParser.ConfigParser()
        parser.read(config_file)
        for prop in props:
            settings[prop] = parser.get('config', prop)
    except:
        pass
    config = Configurator(settings=settings)
    # add static routes
    config.add_static_view('static', 'static', cache_max_age=0)
    # config.add_static_view('/tasks/log/static', 'static', cache_max_age=0)
    # add dynamic routes
    config.add_route('home', '/')
    config.add_route('log', '/log')
    config.add_route('project.create', '/project/create')
    config.add_route('project.edit', '/project/{code}/edit')
    config.add_route('project.index', '/projects')
    config.add_route('project.indexer', '/project/{code}/indexer')
    config.add_route('project.mailer', '/project/{code}/mailer')
    config.add_route('project.view', '/project/{code}')
    config.scan()
    logger.info("Starting application")
    return config.make_wsgi_app()
