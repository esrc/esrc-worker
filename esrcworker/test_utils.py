"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import utils
import unittest


class TestUtils(unittest.TestCase):
    """Test cases for the utils module.
    """

    def setUp(self):
        '''
        Setup the test environment.
        '''
        pass

    def tearDown(self):
        '''
        Tear down the test environment.
        '''
        pass

    def test__arrayToString(self):
        pass

    def test_getConfiguration(self):
        pass

    def test_getDateStampFromFilename(self):
        """
        It should return the date stamp portion of the log file name.
        """
        cases  = [
            ("2013_10_25_16_20_36-ABC-task_list_directory.log", "2013-10-25 16:20:36"),
            ("2012_12_25_12_20_36-TEST-task_list_directory.log", "2012-12-25 12:20:36"),
        ]
        for case in cases:
            filename, date_stamp = case
            result = utils.getDateStampFromFilename(filename)
            self.assertNotEqual(result, None)
            self.assertEqual(result, date_stamp)

    def test_getJobIdFromFilename(self):
        """
        It should return the date stamp portion of the filename.
        """
        cases  = [
            ("2013_10_25_16_20_36-ABC-task_list_directory.log", "2013_10_25_16_20_36"),
            ("2013_10_25_16_20_30-TEST-task_update_graph.log", "2013_10_25_16_20_30"),
        ]
        for case in cases:
            filename, task_id = case
            result = utils.getJobIdFromFilename(filename)
            self.assertNotEqual(result, None)
            self.assertEqual(result, task_id)

    def test_getJobList(self):
        """
        It should return a list of zero or more functions in the task module
        whose name begins with 'task_'.
        """
        count = 6
        tasks = utils.getJobList()
        self.assertNotEqual(tasks, None)
        self.assertGreater(len(tasks), 0)
        self.assertEqual(len(tasks), count)

    def test_getJobName(self):
        pass

    def test_getJobNameFromLogFile(self):
        """
        It should return the name of the task that was executed and produced
        the specified log file.
        """
        cases  = [
            ("2013_10_25_16_20_36-ABC-task_list_directory.log", "list directory"),
            ("2013_10_25_16_20_36-TEST-task_update_graph.log", "update graph"),
            ("2013_10_25_16_20_36-XYZ-task_update_collection_analysis.log", "update collection analysis"),
        ]
        for case in cases:
            filename, task_name = case
            result = utils.getJobNameFromLogFile(filename)
            self.assertNotEqual(result, None)
            self.assertEqual(result, task_name)

    def test_getIndexerConfiguration(self):
        pass

    def test_getLogFilePath(self):
        """
        It should return an absolute log file path, with filename consisting of
        a timestamp, followed by a hyphen, project code, hyphen, then task name.
        """
        cases = [
            ('PROJ', 'task_update_search_index', '-PROJ-task_update_search_index.log'),
            ('PROJABC', 'task_update_search_index', '-PROJABC-task_update_search_index.log')
        ]
        for case in cases:
            proj, task_name, filename_part = case
            log_file_name = utils.getLogFilePath(proj, task_name)
            self.assertNotEqual(log_file_name, None)
            self.assertEquals(filename_part in log_file_name, True)

    def test_getMailerConfiguration(self):
        pass

    def test_getProjectCodes(self):
        pass

    def test_getProjectCodeFromLogFile(self):
        cases  = [
            ("2013_10_25_16_20_36-ABC-task_list_directory.log", "ABC"),
            ("2013_10_25_16_20_36-TEST-task_list_directory.log", "TEST"),
            ("2013_10_25_16_20_36-XYZ-task_list_directory.log", "XYZ"),
        ]
        for case in cases:
            filename, project_code = case
            result = utils.getProjectCodeFromLogFile(filename)
            self.assertNotEqual(result, None)
            self.assertEqual(result, project_code)

    def test_getProjectConfiguration(self):
        pass

    def test_setProjectConfiguration(self):
        pass


if __name__ == "__main__":
    unittest.main()