"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from redis import Redis
from rq import Queue

import ConfigParser
import logging
import os
import subprocess
import jobs as project_jobs
import traceback
import utils

logger = logging.getLogger()

#------------------------------------------------------------------------------
# Views

@view_config(route_name='project.create', renderer='create.mako', request_method="POST")
def handle_create_project(request):
    """Handle create project action.
    """
    try:
        # get user submitted values
        code = request.POST['code']
        title = request.POST['title']
        description = request.POST['description']
        # load project settings
        project_path = request.registry.settings['project_path']
        config_path = request.registry.settings['project_config_path']
        config_file = request.registry.settings['project_config_file']
        # create the project folder
        path = project_path + os.sep + code
        if not os.path.exists(path):
            os.mkdir(path)
        os.mkdir(path + os.sep + 'archive')
        os.mkdir(path + os.sep + 'bin')
        os.mkdir(path + os.sep + 'development')
        os.mkdir(path + os.sep + 'etc')
        os.mkdir(path + os.sep + 'logs')
        os.mkdir(path + os.sep + 'testing')
        # write the project metadata file
        config = ConfigParser.SafeConfigParser()
        config.add_section("project")
        config.set("project", "title", title)
        config.set("project", "description", description)
        with open(project_path + os.sep + config_path + os.sep + config_file, 'wb') as configfile:
            config.write(configfile)
    except:
        return HTTPFound('/project/create')
    return HTTPFound('/')

@view_config(route_name='project.edit', request_method="POST")
def handle_edit_project(request):
    """Handle project edit action.
    """
    code = request.matchdict['code']
    metadata = {}
    metadata['code'] = code
    metadata['title'] = request.POST['title']
    metadata['description'] = request.POST['description']
    utils.setProjectMetadata(request.registry.settings, code, metadata)
    # redirect to the project page
    url = '/project/{0}'.format(code)
    return HTTPFound(url)

@view_config(route_name="project.view", renderer="project.mako", request_method="POST")
def handle_run_job(request):
    """
    Process user submitted data, send the specified jobs to queue, then render
    the job view.
    """
    # get user submitted values
    code = request.matchdict['code']
    email = request.POST['email'].decode('utf-8')
    jobs = []
    for param in request.POST:
        if param.startswith('job_'):
            jobs.append(param)
    # enqueue the jobs
    redis_conn = Redis(host='worker.internal')
    messages = []
    errors = []
    for job in jobs:
        try:
            func = getattr(project_jobs, job)
            cmd, queue_name, timeout = func(code, email, job)
            func = getattr(subprocess, 'call')
            q = Queue(queue_name, connection=redis_conn)
            # q.enqueue(func, cmd, shell=True, timeout=timeout)
            q.enqueue(func, cmd, shell=True)
            msg = "Job '{0}' sent to {1}".format(job, queue_name)
            messages.append(msg)
        except:
            tb = traceback.format_exc()
            msg = 'Error running {0} for project {1}\n{2}'.format(job, code, tb)
            errors.append(msg)
            logger.error(msg)
    # render the page
    metadata = utils.getProjectMetadata(request.registry.settings, code)
    return {
        'code': code,
        'description': metadata['description'],
        'errors': errors,
        'indexer': utils.getIndexerConfiguration(request.registry.settings, code),
        'mailer': utils.getMailerConfiguration(request.registry.settings, code),
        'messages': messages,
        'read_only': True if code in request.registry.settings['read_only_projects'] else False,
        'jobs': utils.getJobList(),
        'title': metadata['title'],
    }

@view_config(route_name='project.indexer', request_method="POST")
def handle_update_indexer_config(request):
    """Update the indexer configuration.
    """
    project_code = request.matchdict['code']
    # redirect to the project page
    url = '/project/{0}'.format(project_code)
    return HTTPFound(url)

@view_config(route_name="project.mailer", request_method="POST")
def handle_update_mailer_config(request):
    project_code = request.matchdict['code']
    config = ConfigParser.ConfigParser()
    config.add_section('mailer')
    config.set('mailer', 'sitename', request.POST['sitename'])
    config.set('mailer', 'from', request.POST['from'])
    config.set('mailer', 'mailto', request.POST['mailto'])
    config.set('mailer', 'subject', request.POST['subject'])
    config.set('mailer', 'thankyou', request.POST['thankyou'])
    config.set('mailer', 'successpage', request.POST['successpage'])
    config.set('mailer', 'errorpage', request.POST['errorpage'])
    # write the updated mailer configuration
    project_path = request.registry.settings['project_path']
    config_path = request.registry.settings['project_config_path']
    mailer_config_file = request.registry.settings['mailer_config_file']
    path = project_path + os.sep + project_code + os.sep + config_path + os.sep + mailer_config_file
    try:
        with open(path, 'w') as configfile:
            config.write(configfile)
        msg = "Updated mailer configuration for {0}".format(project_code)
        logger.info(msg)
    except:
        msg = "Could not update mailer configuration for {0}. Ensure that configuration files are owned by 'www-data/esrc'".format(project_code)
        logger.error(msg, exc_info=True)
    # redirect to the project page
    url = '/project/{0}'.format(project_code)
    return HTTPFound(url)

@view_config(route_name='home')
def render_home(request):
    """
    Default application view redirects to the jobs page.
    """
    url = request.route_url('project.index')
    return HTTPFound(location=url)

@view_config(route_name='log', renderer='log.mako')
def render_log(request):
    """
    Activity log view displays the last 50 events in reverse chronological
    order.
    """
    try:
        log_file_path = request.registry.settings['log_path']
        data = []
        files = [filename for filename in os.listdir(log_file_path) if filename.endswith(".log")]
        files.sort()
        files.reverse()
        for filename in files:
            record = {}
            record['id'] = utils.getJobIdFromFilename(filename)
            record['datestamp'] = utils.getDateStampFromFilename(filename)
            record['project'] = utils.getProjectCodeFromLogFile(filename)
            record['title'] = utils.getJobNameFromLogFile(filename)
            record['output'] = []
            with open(log_file_path + os.sep + filename, 'r') as f:
                for line in f:
                    record['output'].append(line)
            data.append(record)
        return { 'log': data }
    except Exception, e:
        logger.error("Could not render log", exc_info=True)
        return { 'log': 'No log file available' }

@view_config(route_name='project.view', renderer="project.mako", request_method="GET")
def render_project(request):
    """Show the project metadata, jobs page.
    """
    code = request.matchdict['code']
    metadata = utils.getProjectMetadata(request.registry.settings, code)
    jobs = utils.getJobList()
    return {
        'code': code,
        'description': metadata['description'],
        'errors': [],
        'indexer': utils.getIndexerConfiguration(request.registry.settings, code),
        'mailer': utils.getMailerConfiguration(request.registry.settings, code),
        'messages': [],
        'read_only': True if code in request.registry.settings['read_only_projects'] else False,
        'jobs': jobs,
        'title': metadata['title'],
    }

@view_config(route_name='project.create', renderer='create.mako', request_method="GET")
def render_project_create(request):
    """Show project create form.
    """
    return {
        'projects': utils.getProjectCodes(request.registry.settings)
    }

@view_config(route_name='project.edit', renderer='edit.mako', request_method="GET")
def render_project_edit(request):
    """Show project edit form.
    """
    code = request.matchdict['code']
    metadata = utils.getProjectMetadata(request.registry.settings, code)
    return {
        'code': code,
        'title': metadata['title'],
        'description': metadata['description']
    }

@view_config(route_name='project.index', renderer="index.mako", request_method="GET")
def render_project_index(request):
    """Show the list of existing projects and their metadata.
    """
    return {
        'projects': utils.getProjectListWithMetadata(request.registry.settings),
        'jobs': utils.getJobList(),
    }

